# INTRODUCTION

Entity Translation Unified Form allows creation of multilingual node content on one form.

# Configuration

Visit the content language page (admin/config/regional/content-language). Next to a content-translatable entity type, enable "Place all content-translatable fields for all enabled languages inline on the node add/edit form". When that box is checked, a new select box will appear for the "Display Mode".

1) Enable translateable for your content type (leftmost checkbox)
2) Check /enable "Place all content-translateable fields for all enabled languages inline on the entity add/edit form and select "inline mode"
3) Check /enable "Enable side-by-side UI mode on the node add/edit form

Do not disable sync mode unless you really know what you're doing.

remember to also adjust translateability on your "fields" for the content type

recommend using field groups to place non-translateable fields in those groups so they don't whack out the node edit form

recommend to stick with Inline mode.


By default, the Inline Mode will be the selected Display Mode. If the "Display Mode" is set to "Tabbed Mode", please see the requirements below.

# Display Modes

There are two display mode plugins included with this module:

* Inline Mode
* Tabbed Mode

Additional modes can be added using the Drupal plugin system. Copy and extend one of the classes in the src/Plugin/EntityTranslationUnifiedFormMode folder to the same subfolder in your own module to add a mode.

## Inline Mode

In this mode, the additional language fields are displayed after the current language field. Field titles are appended with their language in parentheses. i.e. Title (English)

## Inline Mode with side by side mode

There is a new side-by-side mode, in the content type settings under publishing options enable the checkbox for this mode.
For complex content types, create two form groups in the form display, one a details group and the other a sidebar group, put the non-translateable fields in the details group and move this group underneith the translatable fields, put other fields as you wish in the sidebar group.  This ensures optimal view mode, side by side only works 992px and higher.


## Tabbed Mode

This mode applies the "A11Y Accordion Tabs" js from https://github.com/matthiasott/a11y-accordion-tabs.

Much of the style and integration was taken and adapted from the "A11Y Paragraphs Tabs" Drupal module: https://www.drupal.org/project/a11y_paragraphs_tabs

### Tabbed Mode Requirements

The A11Y Accordion Tabs js library must be placed in the Drupal /libraries path:

* Download the "A11Y Accordion Tabs" js library:
    https://github.com/matthiasott/a11y-accordion-tabs
* Extract and rename it to "a11y-accordion-tabs", so the assets are at:
    /libraries/a11y-accordion-tabs/a11y-accordion-tabs.js

### Installing A11Y Accordion Tabs using composer

In composer.json under repositories, add the following:
```
    {
        "type": "package",
        "package": {
            "name": "matthiasott/a11y-accordion-tabs",
            "version": "0.4.1",
            "dist": {
                "url": "https://github.com/matthiasott/a11y-accordion-tabs/archive/v0.4.1.zip",
                "type": "zip"
            },
            "type": "drupal-library"
        }
    },
```


Then run the following command:
    composer require matthiasott/a11y-accordion-tabs

Also, for the preview functionality to work correctly you must also use the following drupal core patch:
https://www.drupal.org/files/issues/2020-06-20/node-preview_current_language-3150728-12.patch
Please report your results here: https://www.drupal.org/i/3350693
