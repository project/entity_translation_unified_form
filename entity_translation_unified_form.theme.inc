<?php

use Drupal\entity_translation_unified_form\EtufHelper;

/**
 * Prepare field variables for the form wrapper template.
 *
 * Implements hook_preprocess_HOOK().
 */
function template_preprocess_entity_translation_unified_form__inline__wrapper(&$variables) {

}

/**
 * Prepare field variables for the form wrapper template.
 *
 * Implements hook_preprocess_HOOK().
 */
function template_preprocess_entity_translation_unified_form__inline__field_wrapper(&$variables) {

}

/**
 * Prepare field variables for the form wrapper template.
 *
 * Implements hook_preprocess_HOOK().
 */
function template_preprocess_entity_translation_unified_form__a11y_accordion_tabs__wrapper(&$variables) {
  $variables['#attached']['library'][] = 'entity_translation_unified_form/a11y-accordion-tabs';

  $current_language = \Drupal::languageManager()->getCurrentLanguage();

  $id = $variables['element']['#id'];
  $variables['fields'][$id] = array(
    'id' => $id,
    'language_name' => t($current_language->getName()),
    'label' => isset($variables['element']['widget']['#title']) ? $variables['element']['widget']['#title'] : NULL,
    'markup' => $variables['element']['#children'],
  );

  $other_languages = EtufHelper::getOtherEnabledLanguages();
  foreach($other_languages as $langcode => $language) {
    if (isset($variables['element'][$langcode]['#id'])) {
      $id = $variables['element'][$langcode]['#id'];
      $variables['fields'][$id] = array(
        'id' => $id,
        'language_name' => t($language),
        'label' => isset($variables['element'][$langcode]['widget']['#title']) ? $variables['element'][$langcode]['widget']['#title'] : NULL,
        'markup' => isset($variables['element'][$langcode]['#children']) ? $variables['element'][$langcode]['#children'] : NULL,
      );
    }
  }
}

/**
 * Prepare field variables for the form wrapper template.
 *
 * Implements hook_preprocess_HOOK().
 */
function template_preprocess_entity_translation_unified_form__a11y_accordion_tabs__form_wrapper(&$variables) {

}
