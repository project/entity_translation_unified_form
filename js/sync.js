/**
 * @file
 * Drupal etuf object.
 */

/**
 * @namespace
 */
(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.entity_translation_unified_form = {
    settings: drupalSettings.entity_translation_unified_form || {},
    attach: function (context, settings) {
      if (context == document) {
        if ($('body').hasClass('sync')) {
          var otherLangs = drupalSettings.other_langs;
          $(otherLangs).each(function(index, other_lang) {
            var current_lang = $('html').attr('lang');
            //ETUF - sync the publish status selection, with JS
            var multiStatus = $("#edit-moderation-state-0-state, #edit-moderation-state-etuf-"+other_lang+"-0-state, #edit-moderation-state-etuf-"+current_lang+"-0-state");
            var currentStatus = $("#edit-moderation-state-etuf-"+current_lang+"-0-state");
            var otherStatus = $("#edit-moderation-state-etuf-"+other_lang+"-0-state");
            if (typeof otherStatus !== 'undefined') {
              if (typeof currentStatus !== 'undefined' && typeof currentStatus.val() !== 'undefined') {
                otherStatus.val(currentStatus.val());
              }
              else {
                currentStatus = $("#edit-moderation-state-0-state");
                if (typeof currentStatus.val() !== 'undefined' && currentStatus.val().length) {
                  otherStatus.val(currentStatus.val());
                }
              }
            }
            multiStatus.change(function(e) {
              multiStatus.val(this.value); // "this" is the changed one
            });
          });
        }
      }
    }
  };

  /**
   * Returns the version of WxT being used.
   *
   * @return {string}
   *   The version of WxT being used.
   */
  //Drupal.entity_translation_unified_form.version = 'v1.x';


})(window.jQuery, window.Drupal, window.drupalSettings);
