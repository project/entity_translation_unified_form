<?php

namespace Drupal\entity_translation_unified_form\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a EntityTranslationUnifiedFormMode annotation object.
 *
 * @Annotation
 */
class EntityTranslationUnifiedFormMode extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The administrative label of the mode.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $admin_label = '';

}
