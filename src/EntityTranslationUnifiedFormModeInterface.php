<?php

namespace Drupal\entity_translation_unified_form;

interface EntityTranslationUnifiedFormModeInterface {
  //public function validateDependencies
  public function fieldFormAlter($form, $form_state, &$field, $field_name, $language);
  public function getFieldGroupThemeWrapper($form, $form_state, $field, $field_name);
  public function getFieldThemeWrapper($form, $form_state, $field, $field_name, $language);
}
