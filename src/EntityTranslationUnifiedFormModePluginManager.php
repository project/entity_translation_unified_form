<?php

namespace Drupal\entity_translation_unified_form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 *
 */
class EntityTranslationUnifiedFormModePluginManager extends DefaultPluginManager {

  /**
   * Constructs a EntityTranslationUnifiedFormModePluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/EntityTranslationUnifiedFormMode',
      $namespaces,
      $module_handler,
      'Drupal\entity_translation_unified_form\EntityTranslationUnifiedFormModeInterface',
      'Drupal\entity_translation_unified_form\Annotation\EntityTranslationUnifiedFormMode');

    $this->setCacheBackend($cache_backend, 'entity_translation_unified_form_plugins');
  }

}
