<?php

namespace Drupal\entity_translation_unified_form\Plugin\EntityTranslationUnifiedFormMode;

use Drupal\Core\Render\Element;
use Drupal\entity_translation_unified_form\EntityTranslationUnifiedFormModeInterface;

/**
 * @EntityTranslationUnifiedFormMode (
 *   id = "EntityTranslationUnifiedFormInlineMode",
 *   admin_label = @Translation("Inline Mode"),
 * )
 */
class EntityTranslationUnifiedFormInlineMode implements EntityTranslationUnifiedFormModeInterface {

  /**
   *
   */
  public function fieldFormAlter($form, $form_state, &$field, $field_name, $language) {
    $this->alterTitle($form, $form_state, $field, $field_name, $language);
  }

  /**
   *
   */
  public function getFieldGroupThemeWrapper($form, $form_state, $field, $field_name) {
    return 'entity_translation_unified_form__inline__wrapper';
  }

  /**
   *
   */
  public function getFieldThemeWrapper($form, $form_state, $field, $field_name, $language) {
    return 'entity_translation_unified_form__inline__field_wrapper';
  }

  /**
   * Helper function to add the language label to a title.
   */
  protected function alterTitle($form, $form_state, &$field, $field_name, $language) {
    if (!isset($field['widget']) || !is_object($language)) {
      return;
    }
    // Get language display mode.
    $form_object = $form_state->getFormObject();
    $entity = $form_object->getEntity();
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $language_display = entity_translation_unified_form_language_display($entity_type_id, $bundle);

    if ($language_display == 'native') {
      // Get native languages.
      $language_manager = \Drupal::languageManager();
      $native_languages = $language_manager->getNativeLanguages();
      $current_language = $native_languages[$language->getId()];
      // Get language name in its own language.
      $text = ' (' . t($current_language->getName(), [], ['langcode' => $language->getId()]) . ')';
    }
    elseif ($language_display == 'current') {
      $text = ' (' . t($language->getName()) . ')';
    }
    else {
      $text = ' (' . $language->getId() . ')';
    }
    $widget = &$field['widget'];

    $this->addTranslatabilityClue($widget, $text);
  }

  /**
   * Adds a clue about the form element translatability.
   *
   * If the given element does not have a #title attribute, the function is
   * recursively applied to child elements.
   *
   * @param array $element
   *   A form element array.
   *
   *   This was taken and modified from content_translation.
   */
  protected function addTranslatabilityClue(&$element, $suffix) {
    static $fapi_title_elements;
    $done = TRUE;

    // Elements which can have a #title attribute according to FAPI Reference.
    if (!isset($fapi_title_elements)) {
      $fapi_title_elements = array_flip(['managed_file', 'checkbox', 'checkboxes', 'date', 'details', 'fieldset', 'file', 'item', 'password', 'password_confirm', 'radio', 'radios', 'select', 'text_format', 'textarea', 'textfield', 'weight']);
    }

    // Update #title attribute for all elements that are allowed to have a
    // #title attribute according to the Form API Reference. The reason for this
    // check is because some elements have a #title attribute even though it is
    // not rendered; for instance, field containers.
    if (isset($element['#type']) && isset($fapi_title_elements[$element['#type']]) && isset($element['#title'])) {
      $element['#title'] .= $suffix;
      // If we changed a invisible title the job is not done.
      if (isset($element['#title_display']) && ($element['#title_display'] == 'invisible')) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }

    // If the current element does not have a (valid) title, try child elements.
    if ($children = Element::children($element)) {
      foreach ($children as $delta) {
        if (!$this->addTranslatabilityClue($element[$delta], $suffix)) {
          $done = FALSE;
        }
      }
    }
    // If children are not "really" changed, apply to parent (happens with fields with cardinality>1).
    if (!$done && isset($element['#title'])) {
      $element['#title'] .= $suffix;
    }
  }

}
