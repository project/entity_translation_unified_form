<?php

namespace Drupal\entity_translation_unified_form\Plugin\EntityTranslationUnifiedFormMode;

use Drupal\entity_translation_unified_form\EntityTranslationUnifiedFormModeInterface;

/**
 * @EntityTranslationUnifiedFormMode (
 *   id = "EntityTranslationUnifiedFormTabbedMode",
 *   admin_label = @Translation("Tabbed Mode"),
 * )
 */
class EntityTranslationUnifiedFormTabbedMode extends EntityTranslationUnifiedFormInlineMode implements EntityTranslationUnifiedFormModeInterface {

  /**
   *
   */
  public function fieldFormAlter($form, $form_state, &$field, $field_name, $language) {
    if ($field_name == 'path') {
      // Path doesn't currently work well with tabs.
      // use the inline mode for now.
      parent::alterTitle($form, $form_state, $field, $field_name, $language);
    }
    return;
  }

  /**
   *
   */
  public function getFieldGroupThemeWrapper($form, $form_state, $field, $field_name) {
    if ($field_name == 'path') {
      // Path doesn't currently work well with tabs.
      // use the inline wrapper for now.
      return 'entity_translation_unified_form__inline__wrapper';
    }
    return 'entity_translation_unified_form__a11y_accordion_tabs__wrapper';
  }

  /**
   *
   */
  public function getFieldThemeWrapper($form, $form_state, $field, $field_name, $language) {
    if ($field_name == 'path') {
      // Path doesn't currently work well with tabs.
      // use the inline wrapper for now.
      return 'entity_translation_unified_form__inline__field_wrapper';
    }
    return 'entity_translation_unified_form__a11y_accordion_tabs__field_wrapper';
  }

}
